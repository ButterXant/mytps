// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gun.generated.h"

// enum for trigger mode
UENUM()
enum class ETriggerMode : uint8 {
	Automatic,
	Semi
};

UCLASS()
class MYTPS_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Gameplay)
	float Rate = 2.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Gameplay)
	ETriggerMode Trigger = ETriggerMode::Automatic;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimInstance* AnimInstance;

	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category = "Setup")
	bool OnFire();

	bool OnFire_Implementation();
private:
	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;

	float LastFireTime = 0.0f;
};
